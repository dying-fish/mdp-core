package com.mdp.mq.sp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

@Configuration
public class RedisChannelConfig {
	
	@Autowired
	 ChannelConfigProvider configProvider;
	
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory ) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        configProvider.container(container); 
        //这个container 可以添加多个 messageListener
        return container;
    } 
 
}