package com.mdp.mq.sp.def;

import com.mdp.mq.sp.ChannelConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;
import org.springframework.stereotype.Service;

@Service
public class DefaultChanelConfig implements ChannelConfig {

    public static String DEFAULT_TOPIC_NAME="mdp-def-notify-msg";

    RedisMessageListenerContainer container;


    @Autowired
    DefaultMessageListener defaultMessageListener;

    @Override
    public RedisMessageListenerContainer container(RedisMessageListenerContainer container) {
        this.container=container;
        container.addMessageListener(defaultMessageListener, new ChannelTopic(DEFAULT_TOPIC_NAME));
        return container;
    }

    @Override
    public void setMessageListener(MessageListener messageListener, Topic topic) {
        container.addMessageListener(messageListener,topic);
    }

    @Override
    public void removeMessageListener(MessageListener messageListener, Topic topic) {
        container.removeMessageListener(messageListener,topic);
    }

    @Override
    public void removeMessageListener(MessageListener messageListener) {
        container.removeMessageListener(messageListener);
    }

    @Override
    public void removeMessageListener(String listenerName, Topic topic) {
        container.removeMessageListener(defaultMessageListener,topic);
    }

    @Override
    public void addMessageListener(String listenerName, Topic topic) {
        container.addMessageListener(defaultMessageListener,topic);
    }
}
