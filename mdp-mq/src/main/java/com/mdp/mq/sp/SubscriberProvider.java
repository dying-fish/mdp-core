package com.mdp.mq.sp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubscriberProvider  {
	
	@Autowired(required=false)
	List<Subscriber> subscribers;
	
	public void receiveMessage(String channelName,Object message) {
		 if(subscribers!=null && subscribers.size()>0) {
			 for (Subscriber subscriber : subscribers) {
				 subscriber.receiveMessage(channelName,message);
			}
		 }
	}
	
	

}
