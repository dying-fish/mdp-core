package com.mdp.mq.sp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestSp {
	
	@Autowired
	 ImPublish publish;
	
	Logger log= LoggerFactory.getLogger(TestSp.class);
	
	@Test
	public  void testPush() { 
		Date d=new Date();
		 for(int i=0;i<100;i++) {
			 try {
				 publish.push("im", "im message "+i+" 发送时间:"+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		 }
		
	}  
	 
}
