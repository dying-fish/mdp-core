package com.mdp.mq.sp;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.ArrayList;
import java.util.List;

public class ImMessageListener implements MessageListener {

	List<Subscriber> subscribers=new ArrayList<>();
	
	RedisSerializer<String> stringSerializer=RedisSerializer.string();
	
	
	@Override
	public void onMessage(Message message, byte[] pattern) {
		
		System.out.println(message);
		System.out.println(stringSerializer.deserialize(pattern));
		String channelName=stringSerializer.deserialize(pattern);
		if(subscribers!=null && subscribers.size()>0) {
			for (Subscriber sub : subscribers) {
				sub.receiveMessage(channelName,message);
			}
		}
		
	}
	public void addSubscriber(Subscriber subscriber) {
		this.subscribers.add(subscriber);
	}

	 
}
