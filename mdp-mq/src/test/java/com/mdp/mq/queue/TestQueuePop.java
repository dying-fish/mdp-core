package com.mdp.mq.queue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestQueuePop {
	
	@Autowired
	Push push;
	
	@Autowired
	Pop pop;
	
	Logger log= LoggerFactory.getLogger(TestQueuePop.class);
	
	
	@Test
	public  void testPush(){ 
		 for(int i=0;i<100;i++) {
			 push.leftPush("test01",new MsgVo("Ok???"+i)); 
		 }
		
	}  
	
	@Test
	public  void testMessageList(){ 
//		for(int i=0;i<100;i++) {
//			MsgVo rightPop = (MsgVo)pop.rightPop("test01");
//			System.out.println(rightPop.msg);
//		}
		//ArrayList rightPop = pop.range("test01", 0, 500);
		//System.out.println(((MsgVo)rightPop.get(0)).msg);
	}  
	@Test
	public  void testRightPop(){ 
//		for(int i=0;i<500;i++) {
//		MsgVo mv = pop.rightPop("test01");
//		System.out.println(mv.msg);
//		}
		Long size = pop.size("test01");
		System.out.println(size);
	}
	/**
	 * 
	 * @param locationId 门店编号
	 * @param tag 标记状态: 0 未付款,1 付款
	 * @param orderId	订单id
	 * @param orderSn	订单编号
	 * @param userid	用户编号
	 * @param actualPrice	实际付款金额
	 * @param remark	留言
	 */
	public  void pushOrder(String locationId,String orderId,String orderSn,String userid,BigDecimal actualPrice,String remark){ 
		if(!StringUtils.isEmpty(locationId)) {
			Map<String,Object> map = new HashMap<>();	
			map.put("userid", userid);
			map.put("actualPrice", actualPrice);
			map.put("orderSn", orderSn);
			map.put("remark", remark);
			push.leftPush("order_"+locationId,map); 
		}
	}
	@Test
	public  void Push(){ 
			 //push.leftPush("test01",new MsgVo("Ok???"+i)); 
			 this.pushOrder("i8fi87886", "1", "11", "110",new BigDecimal(11), "11");
			 this.pushOrder("i8fi87886", "2", "22", "220",new BigDecimal(22), "22");
			 this.pushOrder("ksvw282f2", "1", "11", "110",new BigDecimal(11), "11");
		
	} 
}
