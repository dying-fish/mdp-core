package com.mdp.core.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.accept.HeaderContentNegotiationStrategy;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class RequestUtils {
	
	public static Logger logger = LoggerFactory.getLogger(RequestUtils.class);
	
	public static ObjectMapper o=new ObjectMapper();
	
	public static boolean isAjaxRequest(HttpServletRequest request){
		boolean is= "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
		if(is==false) {
			HeaderContentNegotiationStrategy header=new HeaderContentNegotiationStrategy();
			try {
				List<MediaType> requestMediaTypes=header.resolveMediaTypes(new ServletWebRequest(request));
				for (MediaType requestMediaType : requestMediaTypes) {
					if(MediaType.APPLICATION_JSON.includes(requestMediaType)){
						return true;
					};
				}
				
			} catch (HttpMediaTypeNotAcceptableException e) {
				 return true;
			}
		}else {
			return true;
		}
		return false;
	}
	
	public static  HttpEntity<Map<String,Object>> jsonMapHttpEntity(Map<String,Object> request){
	   	 HttpHeaders headers = new HttpHeaders();
	        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
	        headers.setContentType(type); 
	        HttpEntity<Map<String,Object>> entity = new HttpEntity<Map<String,Object>>(request,headers);
			return entity;
	}
	
	public static void transformArray(Map<String,Object> params,String key) {
		String newKey=key+"[]";
		if(params!=null ) {
			if( params.containsKey(newKey)) {
				params.put(key, getRequest().getParameterMap().get(newKey));
				params.remove(newKey);
			}else if( params.containsKey(key)) { 
				String value=(String) params.get(key);
				if(!StringUtils.hasText(value)) {
					return;
				}
				if(value.indexOf("[")==0) {
					try {
						String[] valus=o.readValue(value, String[].class);
						params.put(key, valus);
					} catch (JsonParseException e) {
						logger.error("",e);
					} catch (JsonMappingException e) {
						logger.error("",e);
					} catch (IOException e) {
						logger.error("",e);
					}
					 
				}else {
					params.put(key, value.split(",")); 
				}
				
			}
			
		}
	}

	/**
	 * 得到request对象
	 */
	public static HttpServletRequest getRequest() {
		RequestAttributes requestAttributes=RequestContextHolder.getRequestAttributes();

		if (requestAttributes!= null) {
			HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
			return request;
		}
		return null;

	}
	
	public static String getIpAddr(HttpServletRequest request) { 
	       String ip = request.getHeader("x-forwarded-for"); 
	       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	           ip = request.getHeader("Proxy-Client-IP"); 
	       } 
	       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	           ip = request.getHeader("WL-Proxy-Client-IP"); 
	       } 
	       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	           ip = request.getRemoteAddr(); 
	       }
	        
	       if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	           ip = request.getHeader("HTTP_CLIENT_IP");
	       }
	       if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	           ip = request.getHeader("HTTP_X_FORWARDED_FOR");
	       }
	       if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	           ip = request.getHeader("X-Forwarded-For");
	       }

	       if(ip.contains(",")){
	    	   String[] ips=ip.split(",");
	    	   ip=ips[0];
	       }
	       return ip; 
	   } 
	
}