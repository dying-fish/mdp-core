package com.mdp.core.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdp.core.api.Sequence;
import com.mdp.core.ctrl.resolver.BizExceptionResolver;
import com.mdp.core.log.MdpLogFilter;
import com.mdp.core.service.SequenceService;
import com.mdp.core.utils.BaseUtils;
import com.mdp.filter.SetCharacterEncodingFilter;
import com.mdp.filter.XssFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;
@ComponentScan(basePackages={"com.mdp"})
@Configuration
@EnableConfigurationProperties(MdpProperties.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MdpAutoConfiguration {

	Logger log= LoggerFactory.getLogger(MdpAutoConfiguration.class);

	private MdpProperties properties;

	public MdpProperties getProperties() {
		return properties;
	}
	@Autowired(required = false)
	public void setProperties(MdpProperties properties) {
		this.properties = properties;
	}


	@Bean
	BizExceptionResolver bizExceptionResolver(){
		BizExceptionResolver bizExceptionResolver=new BizExceptionResolver();
		return bizExceptionResolver;
	}

	@Bean
    public SessionLocaleResolver sessionLocaleResolver2() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        // 默认语言
        slr.setDefaultLocale(Locale.CHINESE);
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor2() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        // 参数名
        lci.setParamName("lang");
        return lci;
    }



	@ConditionalOnMissingBean(SequenceService.class)
	@Bean
	public SequenceService sequenceService(){
		return new SequenceService();
	}

	@ConditionalOnMissingBean(Sequence.class)
	@Bean
	public Sequence sequence(){
		return new SequenceService();
	}

	@Bean
	public FilterRegistrationBean mdpXssFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new XssFilter());//添加过滤器
		registration.addUrlPatterns("/*");//设置过滤路径，/*所有路径
		registration.setName("mdpXssFilter");//设置优先级
		registration.setOrder(Ordered.HIGHEST_PRECEDENCE);//设置优先级
		return registration;
	}

    @Bean
    public FilterRegistrationBean mdpSetCharacterEncodingFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SetCharacterEncodingFilter());//添加过滤器
        registration.addUrlPatterns("/*");//设置过滤路径，/*所有路径
        registration.setName("mdpSetCharacterEncodingFilter");//设置优先级
        registration.setOrder(1);//设置优先级
        return registration;
    }

	@Bean
	public FilterRegistrationBean mdpLogFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new MdpLogFilter());//添加过滤器
		registration.addUrlPatterns("/*");//设置过滤路径，/*所有路径
		registration.setName("mdpLogFilter");//设置优先级
		registration.setOrder(Ordered.HIGHEST_PRECEDENCE);//设置优先级
		return registration;
	}
	@Autowired
	public void setRedisTemplateProperties(RedisTemplate redisTemplate) {
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
	}
	@Autowired
    public void setObjectMapperProperties(ObjectMapper objectMapper ) {

    	objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    	objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
    	objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		BaseUtils.initObjectMapper(objectMapper);
    }
    @Bean
	@ConditionalOnMissingBean(RestTemplate.class)
    RestTemplate restTemplate(){
		RestTemplate restTemplate=new RestTemplate();
		return restTemplate;
	}
}
