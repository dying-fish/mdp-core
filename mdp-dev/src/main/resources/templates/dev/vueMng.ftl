<template>
    <#setting number_format="#">
	<section class="border padding">
		<el-row>
            <#list primaryKeysList as column>
			<el-input v-model="filters.${column.camelsColumnName}" style="width: 20%;" placeholder="${column.remarks}查询 输入 *字符* >10 <9 等" clearable title="支持>、<、 >=、<=、!=、*字符*、$IS NULL、$IN 1,2,3、$between 1,5等操作符"/>
            </#list>
 			<el-button v-loading="load.list" :disabled="load.list==true" @click="searchTableDatas()" icon="el-icon-search" type="primary">查询</el-button>
			<span style="float:right;" v-if="currOpType=='mng'">
                <el-button :disabled="disabledJudge('addBtn') || !checkBtnQx('addBtn',menuDefId) " type="primary" @click="openForm({parentOpType:currOpType,subOpType:'add',formData:addForm,title:'新增'})" icon="el-icon-plus"/>
                <el-button :disabled="disabledJudge('delBtn') || !checkBtnQx('delBtn',menuDefId) || this.sels.length===0 || load.del==true" type="danger" v-loading="load.del" @click="batchDel" icon="el-icon-delete"/>
            </span>
            <span style="float:right;" v-else-if="currOpType=='select' &&  this.multiple==true">
                <el-button :disabled="disabledJudge('selectBtn') || this.sels.length===0" type="primary" @click="selectListConfirm" icon="el-icon-check"/>
            </span>
		</el-row>
		<el-row>
            <mdp-hi-query :column-configs="columnConfigs" v-model="hiQueryParams" @change="onHiQueryParamsChange"/>
            <el-button type="text" icon="el-icon-zoom-out" @click="searchReset()">重置查询</el-button>&nbsp;&nbsp;&nbsp;
		</el-row>
		<el-row>
			<!--列表 ${entityName} ${tableRemarks}-->
			<el-table :ref="refId+'Table'" :height="100" v-adaptive="{bottomOffset: bottomOffset}" :data="tableDatas" @sort-change="sortChange" highlight-current-row v-loading="load.list" border @selection-change="selsChange" @row-click="rowClick" style="width: 100%;">
				<el-table-column  type="selection" width="55" show-overflow-tooltip fixed="left" v-if="currOpType=='mng' || this.multiple==true"></el-table-column>
				<el-table-column sortable type="index" width="55" show-overflow-tooltip  fixed="left"></el-table-column>
				<#list primaryKeysList as column>
				<el-table-column prop="${column.camelsColumnName}" label="${column.remarks}" min-width="120" show-overflow-tooltip  fixed="left" col-type="${column.simpleJavaClassName}" v-if="showCol('${column.camelsColumnName}')"></el-table-column>
				</#list>
				<#list columnExcludePkList as column>
				<el-table-column prop="${column.camelsColumnName}"  label="${column.remarks}" min-width="120" show-overflow-tooltip col-type="${column.simpleJavaClassName}" v-if="showCol('${column.camelsColumnName}')">
				    <template slot-scope="scope">
				        <#if column.simpleJavaClassName=='String'>
                            <mdp-input show-style="tag" v-model="scope.row.${column.camelsColumnName}" :maxlength="${column.columnSize}" @change="editSomeFields(scope.row,'${column.camelsColumnName}',$event)" :disabled="!editable || disabledJudge('${column.camelsColumnName}')"/>
                        <#elseif column.simpleJavaClassName=='Date'>
                            <mdp-date type="date" placeholder="选择日期" show-style="tag" v-model="scope.row.${column.camelsColumnName}" @change="editSomeFields(scope.row,'${column.camelsColumnName}',$event)"  value-format="yyyy-MM-dd HH:mm:ss" format="yyyy-MM-dd" :disabled="!editable || disabledJudge('${column.camelsColumnName}')"/>
                        <#elseif column.simpleJavaClassName=='boolean' || column.simpleJavaClassName=='Boolean'>
                            <mdp-select type="radio" show-style="tag" v-model="scope.row.${column.camelsColumnName}" @change="editSomeFields(scope.row,'${column.camelsColumnName}',$event)" :disabled="disabledJudge('${column.camelsColumnName}')" :options="[{id:'0',name:'否'},{id:'1',name:'是'}]"/>
                        <#elseif column.simpleJavaClassName=='Number'>
                            <mdp-number show-style="tag" v-model="scope.row.${column.camelsColumnName}" @change="editSomeFields(scope.row,'${column.camelsColumnName}',$event)" :disabled="!editable || disabledJudge('${column.camelsColumnName}')"/>
                        <#elseif column.simpleJavaClassName=='Integer' || column.simpleJavaClassName=='int'>
                            <mdp-number show-style="tag" v-model="scope.row.${column.camelsColumnName}" @change="editSomeFields(scope.row,'${column.camelsColumnName}',$event)" :precision="0" :disabled="!editable || disabledJudge('${column.camelsColumnName}')"/>
                        <#else>
                            <mdp-input show-style="tag" v-model="scope.row.${column.camelsColumnName}" @change="editSomeFields(scope.row,'${column.camelsColumnName}',$event)" :maxlength="${column.columnSize}" :disabled="!editable || disabledJudge('${column.camelsColumnName}')"/>
                        </#if>
				    </template>
				</el-table-column>
				</#list>
				<el-table-column label="操作" :width="currOpType=='mng'?380:180" fixed="right">
				    <template slot="header" slot-scope="scope">
                        <el-button icon="el-icon-download" @click="export2Excel()">导出</el-button>
                        <mdp-table-configs :column-configs="columnConfigs" v-model="checkedColumns"/>
                    </template>
				    <template scope="scope" v-if="currOpType=='mng'"  >
				        <el-button :disabled="disabledJudge('editBtn') || !checkBtnQx('editBtn',menuDefId) " type="primary" @click="openForm({parentOpType:currOpType,subOpType:'edit',formData:scope.row,title:'修改'})" icon="el-icon-edit" title="修改一条数据"/>
				        <el-button :disabled="disabledJudge('addBtn') || !checkBtnQx('addBtn',menuDefId) " type="success" @click="copy(scope.row)" icon="el-icon-document-copy" title="拷贝并新增一条除了主键不一样其它都一样的数据"/>
				        <el-button :disabled="disabledJudge('delBtn') || !checkBtnQx('delBtn',menuDefId) " type="danger" @click="handleDel(scope.row,scope.$index)" icon="el-icon-delete"  title="删除一条数据"/>
				    </template>

				    <template scope="scope" v-else-if="currOpType=='list'" >
				        <el-button   type="primary" @click="openForm({parentOpType:currOpType,subOpType:'detail',formData:scope.row})" icon="el-icon-view"/>
 				    </template>

				    <template scope="scope" v-else-if="currOpType=='select' && this.multiple!=true" >
				        <el-button   type="primary" @click="selectConfirm(scope.row)" icon="el-icon-check"/>
 				    </template>
				</el-table-column>
			</el-table>
		</el-row>
		<el-row>
			<el-pagination
				layout="slot,total, sizes, prev, next,pager,jumper"
				@current-change="handleCurrentChange"
				@size-change="handleSizeChange"
				:page-sizes="[10,20, 50, 100, 500]"
				:current-page="pageInfo.pageNum"
				:page-size="pageInfo.pageSize"
				:total="pageInfo.total"
				style="float:right;"
			>
			</el-pagination>
		</el-row>
		<el-row>
			<!--新增修改明细 ${entityName} ${tableRemarks}界面-->
            <mdp-dialog :ref="refId+'FormDialog'">
                <template v-slot="{visible,data,dialog}">
                     <${h5SmallEntityName}-form :ref="refId+'Form'" :visible="visible" :parent-op-type="currOpType" :sub-op-type="data.subOpType" :form-data="data.formData" @close="onFormClose" @submit="afterFormSubmit" @fields-change="afterEditSomeFields"/>
                </template>
            </mdp-dialog>
 	    </el-row>
	</section>
</template>

<script>

import { MdpTableMixin } from '@/components/mdp-ui/mixin/MdpTableMixin.js';
import * as ${entityName}Api from '@/api/${apiPath}/${smallEntityName}';
import  ${entityName}Form from './Form';//新增修改明细界面
import { mapGetters } from 'vuex'

export default {
    name:'${smallEntityName}Mng',
    mixins:[MdpTableMixin],
    components: {
        ${entityName}Form,
    },
    computed: {
    },
    watch:{
    },
    data() {
        return {
            menuDefId:'',//menu_def.id 菜单表菜单编号，用于按钮权限判断
            menuDefName:'${tableRemarks}',//menu_def.name 功能名称，用于导出excel等文件名
            refId:'${smallEntityName}',//引用编号，<table :ref="refId+'Table'"> <form :ref="refId+'Form'">
            pkNames:[<#list primaryKeysList as column>"${column.camelsColumnName}"<#if column_has_next>, </#if></#list>],//表格主键的java属性名称，驼峰命名，默认为id,支持多主键
            currOpType:'mng',//表格 mng-综合管理具有最大权限，所有按钮可动、detail-只看不能操作
            filters:{//查询参数

            },
            defaultFilters:{//默认查询参数,第一次打开界面的时候用到，恢复默认值的时候用到

            },

            defaultCheckColumnNum:8,//默认展示的表格列数，前8列

            editable:true,//是否可编辑模式

            //增删改查(含批量)接口
            apis:{
                list: ${entityName}Api.list${entityName},
                add: ${entityName}Api.add${entityName},
                del: ${entityName}Api.del${entityName},
                edit: ${entityName}Api.edit${entityName},
                editSomeFields: ${entityName}Api.editSomeFields${entityName},
                batchAdd: ${entityName}Api.batchAdd${entityName},
                batchDel: ${entityName}Api.batchDel${entityName},
                batchEdit: ${entityName}Api.batchEdit${entityName},
            }
        }
    },
    methods: {
         //页面初始化需要配置的特殊逻辑写这里
          initCurrData(){
             this.searchTableDatas();
          },

          /**
           * 检查参数是否满足调用后台接口的条件
           *
           * @param params 提交给后台的参数池,map类型
           * @returns true / false
           */
          preQueryParamCheck(params){
              return true;
          },

          //页面数据加载完后需要对数据进行加工处理的
          afterList(tableDatas,isOk,apiName){

          },

          /**
           * 对修改的字段进行判断，返回false ,将取消更新数据库
           * @param {*} row 当前选中的行
           * @param {*} fieldName 修改的字段名
           * @param {*} $event 修改后的值
           * @param {*} params 将要提交服务器的参数
           * @returns true/false 返回false ,将取消更新数据库
           */
          editSomeFieldsCheck(row,fieldName,$event,params){
              if(this.currOpType=='add'||this.currOpType=='detail'){
                  return false;
              }
              params[fieldName]=$event
              return true;
          },
    },
    mounted() {

    }
}

</script>

<style scoped>
</style>