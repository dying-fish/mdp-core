![唛盟-mdp](static/images/maimeng_font_logo.png) 
<p align="center">
	<strong>唛盟-mdp:多功能、高效率、低代码的前后端一体化、智能化的开发平台</strong>
</p>

<p align="center">
	<a target="_blank" href="https://gitee.com/qingqinkj/mdp-lcode-ui-web">
        <img src='https://gitee.com/qingqinkj/mdp-lcode-ui-web/badge/star.svg?theme=gvp' alt='gitee star'/>
    </a>
 	<a target="_blank" href="https://github.com/qingqinkj218/mdp-lcode-ui-web">
		<img src="https://img.shields.io/github/stars/qingqinkj218/mdp-lcode-ui-web.svg?style=social" alt="github star"/>
    </a>  
</p>
<p align="center">
	👉 <a target="_blank" href="https://maimengcloud.com/lcode/m1/">https://maimengcloud.com/lcode/m1</a>  👈
</p>

## 快速导航 [唛盟-mdp](https://gitee.com/qingqinkj/mdp-core) 
- [前端组件](https://e.gitee.com/qingqinkj/repos/qingqinkj/mdp-lcode-ui-web/sources)
- [后端服务](https://e.gitee.com/qingqinkj/repos/qingqinkj/mdp-lcode-backend/sources)  
- [体验环境](https://maimengcloud.com/lcode/m1/) 
  登陆界面上选择演示账号登陆或者直接扫码登陆，无须注册

## 😭 日常开发中，您是否有以下痛点？

- **团队中缺乏企业级前后端分离的开发底座**，需要在各种框架中进行摸索、整合。
- 重复造轮子现象严重、浪费人力、对开发者经验要求过高。
- 缺乏统一的开发模式，缺乏公共组件的抽取和共享机制，导致业务代码混乱不堪、代码臃肿、bug多、维护困难
- 缺乏统一的足够灵活的权限管理机制，开发人员不得不写一堆的权限代码混入业务代码中，前端权限、后端权限控制混乱不堪
- 缺乏统一的能够覆盖前后端的、满足前后端分离的代码生成器，代码模板无法按企业现状进行重新编辑、修改
- 缺乏统一的编程规范，或者具有书面编程规范，难以贯彻落实到开发中，代码还是五花八门
- 缺乏统一的元数据(数据字典)管理机制，前后的数据共享调用困难，下拉列表数据混乱不堪
- 缺乏统一的流程管理机制，要想进行流程类业务开发非常困难
- 缺乏统一的国际化机制，国际化实施困难，不得不针对各种语言发布多个版本，无法解决后端国际化、前端国际化等问题
- 缺乏统一的微服务、分布式系统整合机制，微服务互相调用、微服务的权限管理困难
- 缺乏统一的认证中心，单点登录实施困难
- 缺乏统一的支付整合机制，接入微信、支付宝、paypal等困难
- 缺乏项目管理工具，项目计划、任务委派、质量管理、需求管理、持续集成等完全没概念 
- **让 唛盟-mdp 来帮你解决这些痛点吧！然而，这些只是 唛盟-mdp 解决的最基础的功能。**

## 😁 为什么要使用 [唛盟-mdp](https://gitee.com/qingqinkj/mdp-core) 

- 完全开源、永久免费的企业级开发底座
	1. 使用mdp能够带来开发效率的大幅提升，代码行数大幅减少，质量提升明显
	2. 使用mdp能够大幅度降低对开发人员的经验要求，大幅度降低人力成本
	3. mdp对各种开源组件进行了融合改进，提供了针对企业开发中各种问题的最佳解决方案
    4. 企业使用一套开源软件即同时拥有前端开发框架及后端开发框架

- 统一的开发模式
	1. 前后端分离
    2. 前后端都分别进行了技术组件、业务组件的抽取、共享，企业可以进行再提炼、抽象，形成更多的公共组件，对后续开发形成强力的支撑作用

- 足够灵活的权限管理机制
    1. 前端提供统一的按钮级别的权限判断接口、提供路由菜单的权限控制机制
    2. 后端实现api接口的自动注册、自动审核
    3. 基于岗位-部门-角色-菜单及按钮-后端api-人员 6要素的权限管理机制，可以0编程实现绝大多数的权限需求

- 基于领域驱动设计(DDD)的框架及代码生成器
    1. 代码生成器覆盖前端、后端，支持任意时刻的重新生成，支持命令行、开发工具插件、在线三种方式生成代码，生成的代码可以0编程使用
    2. 代码生成器代码模板可以按企业现状进行修改、满足不断发展、持续改进的需求
    3. 支持多个表一次性生成，也就是可以一次性生成几十到几百张表的增删改查功能，而开发人员仅需要填写表名即可完成

- 提供完整的编程规范说明
    1. mdp的框架提供了完备的接口说明、组件说明、组件使用场景等
    2. mdp维护团队提供在线支持，及时解答、解决开发者使用过程中的问题

- 提供强大的元数据(数据字典)管理机制
    1. 内置了元数据管理模块，并实现了元数据的分布式缓存、客户端缓存、元数据分发、缓存清理等
    2. 开发者在客户端、任意微服务中、任意单体应用中可以快速获取元数据
    3. 元数据的调用效率等同于调用本地map缓存，几乎可以忽略使用元数据的性能开销问题

- 整合了最新版本的强大的flowable工作流引擎
    1. 基于mdp框架重新开发了流程中心、任务中心、流程的发布、上下架等功能
    2. 提供分布式环境下的流程调用、流程整合问题的解决方案
    3. 提供在线流程设计器，并整合了mdp的权限机制

- 提供强大的国际化解决方案
    1. 前后端均支持分别进行国际化
    2. 多语言的支持与业务代码完全解耦，彻底解决硬编码进行语言切换的问题

- 整合了强大的微服务框架
    1. mdp平台任意组件均同时支持微服务环境、单体应用环境运行，开发人员开发的时候可以以单体应用的方式开发，然后以微服务方式发布到生产、测试环境
    2. 提供微服务的治理

- 强大的DAO层
    1. 支持基于xml文件的sql编写
    2. 支持无xml方式的数据访问
    3. 支持多主键（对mybatis plus进行升级，解决了多主键、多表联合查询等问题）
    4. 支持多数据源，通过备注实现数据源切换
    5. 支持前端构建任意复杂的查询条件并提供对应的最佳实践，支持前端输入框输入>,=,*,$IS NULL,$IN,$NOT IN等运算操作符，支持前端通过 or and 连接符构建任意复杂的条件表达式

- 强大的web ui
    1. 提供好用好看的ui组件库
    2. 提供页面高级查询功能、可以组装任意复杂的查询条件
    3. 提供针对元数据(数据字典)的引用、针对任意表的引用的组件库
    4. 提供导入、导出等基础功能
    5. 提供按钮权限判断接口、
    6. 提供动态菜单功能 

- 提供自定义表单解决方案
    1. 自定义表单设计
    2. 表单展现
    3. 表单数据管理
    4. 自定义表单与工作流整合使用
    5. 自定义表单发布成普通菜单

- 整合了微信支付、支付宝支付、paypal支付
    1. 提供支付、订单、支付通知底层框架，可以快速整合各种支付功能
    2. 整合了微信支付功能，进行配置文件更新即可使用
    3. 整合了支付宝支付功能，进行配置文件更新即可使用
    4. 整合了paypal支付，进行配置文件更新即可使用

- 整合了oauth2.0框架
    1. 提供oauth2.0的整体框架，构建统一认证中心、单点登录等不再是难事
    2. 整合了微信、支付宝、手机验证码、账户密码等登录方式

## ⌨ 技术栈 [唛盟-mdp](https://gitee.com/qingqinkj/mdp-core) 

- 前端 vue全局桶 + element-ui + axios

- 后端 spring boot + spring cloud + spring security + mybatis plus + JWT + flowable

## 💻 样例项目 [唛盟-mdp](https://gitee.com/qingqinkj/mdp-core) 

- [低代码平台] (https://maimengcloud.com/lcode/m1/)
- [系统管理] (https://maimengcloud.com/sys/m1/)
- [协同办公] (https://maimengcloud.com/oa/m1/)
- [项目管理] (https://maimengcloud.com/xm/m1/)
- [流程管理] (https://maimengcloud.com/workflow/m1/)

## ⚙ 快速开始 [唛盟-mdp](https://gitee.com/qingqinkj/mdp-core)
### 前序准备
>⚠️注意：开发mdp应用，一般涉及以下四个项目，需要提前下载到对应的开发环境中
- [mdp-core](https://gitee.com/qingqinkj/mdp-core) 属于mdp基础java库，不写业务代码，一般由公司架构师团队管控，仅作为企业的底层抽象及公共工具类库.如果企业不打算用mdp作为企业研发底座，而是一次性使用，此工程可以不下载，使用官方发行版即可    
- [mdp-lcode-backend](https://gitee.com/qingqinkj/mdp-lcode-backend) 属于构建于mdp-core上的业务系统，包含一般业务系统的基础功能，若需要添加功能，应在此项目上添加具体业务模块，或者复制形成新的业务系统，比如oa-backend等
- [mdp-lcode-ui-web](https://gitee.com/qingqinkj/mdp-lcode-ui-web) 属于mdp的前端项目，其所有后端服务由**mdp-lcode-backend**提供,若需要添加功能，应在此项目上添加具体业务模块，或者复制该项目形成新的业务系统，比如oa-ui-web
- [mdp-code-generator](https://gitee.com/qingqinkj/mdp-code-generator) 代码生成器负责把前端代码创建到mdp-lcode-ui-web中，把后端代码创建到mdp-lcode-backend中

### 前端
>⚠️注意：该项目使用 element-ui@2.3.0+ 版本，所以最低兼容 vue@2.5.0+
>你的本地环境需要安装 [node](http://nodejs.org/) 和 [git](https://git-scm.com/)。我们的技术栈基于 [ES2015+](http://es6.ruanyifeng.com/)、[vue](https://cn.vuejs.org/index.html)、[vue-router](https://router.vuejs.org/zh-cn/) and [element-ui](https://github.com/ElemeFE/element)，提前了解和学习这些知识会对使用本项目有很大的帮助。

#### 开发
```bash
# 克隆项目
git clone https://gitee.com/qingqinkj/mdp-lcode-ui-web.git

# 安装依赖
npm install

# 建议不要用cnpm安装 会有各种诡异的bug 可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```
浏览器访问 http://localhost:8015

#### 发布
```bash
# 构建测试环境
npm run build:sit

# 构建生成环境
npm run build:prod
```

### 后端
>⚠️注意：所谓后端一般指java单体应用、微服务等
#### 初始化开发环境
```bash
# 克隆底层框架
git clone https://gitee.com/qingqinkj/mdp-core.git

# 编译构建基础库 进入mdp-core根目录 
mvn install

# 克隆项目
git clone https://gitee.com/qingqinkj/mdp-lcode-backend.git

# 安装数据库 在数据库创建adm用户，找到 mdp-lcode-backend/mdp-lcode/database/adm_*.sql
将 adm.*.sql导入到数据库中,并到mdp-lcode/src/main/resources/application-dev.yml修改数据源相关用户名,密码,连接地址等

```
#### 开发
```bash
# 运行spring boot项目 找到mdp-lcode-backend/mdp-lcode/src/test/java/com/mdp/SysApplication.java
运行 SysApplication
```

#### 构建
```bash
# 编译构建基础库(如果有修改) 进入mdp-core根目录 
mvn install

# 构建发布包 切换到 mdp-lcode-backend/下，执行下面命令，成功后在 mdp-lcode-bootstrap及mdp-lcode-cloud-bootstrap下都会有jar输出
mvn install

```

#### 发布
>⚠️注意：[bootstrap-mdp-lcode.sh](https://gitee.com/qingqinkj/mdp-core/blob/master/bin/start-service/bootstrap-mdp-lcode.sh)  一般在mdp-lcode-backend/mdp-lcode-bootstrap/bin/start-service/下

```bash
# 将jar包发布到maven服务器 切换到 mdp-core/下
mvn deploy

# 将jar包发布到maven服务器 切换到 mdp-lcode-backend/下
mvn deploy

# 如果是单体运用，将mdp-lcode-backend/mdp-lcode-bootstrap/mdp-lcode-bootstrap-2.0.0-RELEASE.jar拷贝到目标服务器
sh bootstrap-mdp-lcode.sh
# 如果是微服务环境，将mdp-lcode-backend/mdp-lcode-cloud-bootstrap/mdp-lcode-bootstrap-2.0.0-RELEASE.jar拷贝到目标服务器
sh bootstrap-mdp-lcode.sh

```

### 🔔️ 特别提醒

mdp 3.0 版本已经开始规划更新了，尽请期待新版本的诞生吧

### 🗒️ [版本更新日志](https://gitee.com/qingqinkj/mdp-core/blob/master/docs/CHANGELOG.md)

升级前必看：[CHANGELOG.md](https://gitee.com/qingqinkj/mdp-core/blob/master/docs/CHANGELOG.md)

## 🛠️ 整体架构 [唛盟-mdp](https://gitee.com/qingqinkj/mdp-core) 
**mdp框架**经过无数项目的洗礼，对经常遇到的问题的解决办法抽象形成公共模块，以下模块默认内置于底层框架中，

### ✅ 前端ui框架以[mdp-lcode-ui-web](https://gitee.com/qingqinkj/mdp-lcode-ui-web)作为项目模板，提供以下组件库：
>⚠️ 注意：ui库基于elementui进行改造
#### [mdp-dialog](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpDialog) 弹框，可以把任意页面装配成弹框，无须定义多余的变量及函数
#### [mdp-table](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpTable) 表格，内置了增、删、改、查、高级查询、重置查询、导出、列配置、分页、批量编辑等功能、内置了对按钮权限的控制机制
#### [mdp-select](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpSelect) 下拉列表，支持对数据字典、元数据的引用，支持对任意小表表格数据的引用，支持参数化加载后台数据，对后台加载的数据进行缓存
#### [mdp-select-table](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpSelectTable) 超大表格下拉列表，与mdp-select相比，该组件具有分页查询功能
#### [mdp-select-user](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/MdpExt/MdpSelectUser) 用户选择下拉列表，与mdp-select-table组件类似，仅仅针对用户的头像做了特殊处理
#### [mdp-input](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpInput) 输入框
#### [mdp-date](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpDate) 日期
#### [mdp-daterange](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpDateRange) 区间日期
#### [mdp-number](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpNumber) 数字输入
#### [mdp-hi-query](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpHiQuery) 高级查询，可以由用户自定义任意复杂的查询条件
#### [mdp-table-configs](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpTableConfigs) 表格配置，用于控制表格的列显示与否
#### [mdp-transfer](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/Mdp/MdpTransfer) 穿梭框
#### [mdp-images](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/MdpExt/MdpImages) 图片库,支持图片的上传下载、上传后的统一管理、共享
#### [mdp-rich-text](https://gitee.com/qingqinkj/mdp-lcode-ui-web/tree/master/src/components/MdpExt/MdpRichText) 富文本编辑器，整合了mdp-images作为插件

### 后端底层框架[mdp-core](https://gitee.com/qingqinkj/mdp-core)
>⚠️ 注意：以下模块以**spring boot**为基础框架
#### [mdp-boot-starter](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-boot-starter) 启动器
#### [mdp-dao-mybatis](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-dao-mybatis) dao层
#### [mdp-mybatis-enhance](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-mybatis-enhance) 对mybaits的底层扩展
#### [mdp-qx-api](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-qx-api) 权限底层
#### [mdp-ext-utils](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-ext-utils) 扩展的静态工具类
#### [mdp-ds](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-ds) 多数据源底层
#### [mdp-spring-enhance](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-spring-enhance) 对spring底层拓展
#### [mdp-swagger-enhance](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-swagger-enhance) 接口文档拓展，支持对map类型的接口
#### [mdp-tomcat-enhance](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-tomcat-enhance) 对tomcat的拓展
#### [mdp-meta-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-meta-client) 元数据客户端
#### [mdp-micro-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-micro-client) 微服务客户端
#### [mdp-email-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-email-client) 邮件客户端
#### [mdp-msg-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-msg-client) 消息客户端,可向im等发消息
#### [mdp-plat-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-plat-client) 平台配置客户端
#### [mdp-products](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-products) 业务产品的上级pom项目，所有基于mdp的项目，继承该项目
#### [mdp-safe-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-safe-client) 安全相关客户端，获取登录用户的信息，权限判断等
#### [mdp-safe-license](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-safe-license) 应用的license
#### [mdp-sensitive-word](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-sensitive-word) 敏感词过滤
#### [mdp-sms-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-sms-client) 短信客户端
#### [mdp-tpa-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-tpa-client) 第三方登录等客户端
#### [mdp-mq](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-mq) mq封装
#### [mdp-audit-log-client](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-audit-log-client) 接口调用日志记录
#### [mdp-dev](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-dev) 代码生成器底层

## 🚀 基于mdp框架拓展的开源项目
>⚠️ 注意：以下拓展的框架或者子系统，由mdp开源团队基于项目经验进行总结抽取，可用可不用，可以以jar包方式合并到现有工程进行发布，也可单独发布成单体应用、微服务应用

- 第三方支付框架,整合了微信支付、支付宝支付、paypal支付
  1. [mdp-tpa-pay-backend](https://gitee.com/qingqinkj/mdp-tpa-pay-backend) 后端

- 即时通讯框架
  1. [mdp-im-uniapp](https://gitee.com/qingqinkj/mdp-im-uniapp) 前端app
  2. [mdp-im-web](https://gitee.com/qingqinkj/mdp-im-web) 前端浏览器
  3. [mdp-im-backend](https://gitee.com/qingqinkj/mdp-im-backend) 后端

- 统一认证中心框架
  1. [mdp-lcode-ui-web](https://gitee.com/qingqinkj/mdp-lcode-ui-web) 前端 
  2. [mdp-oauth2-backend](https://gitee.com/qingqinkj/mdp-oauth2-backend) 后端
  3. [mdp-lcode-backend](https://gitee.com/qingqinkj/mdp-lcode-backend) 后端

- 微服务框架
  1. [mdp-cloud-backend](https://gitee.com/qingqinkj/mdp-cloud-backend) 后端

- 工作流管理子系统
  1. [mdp-workflow-ui-web](https://gitee.com/qingqinkj/mdp-workflow-ui-web) 前端 
  2. [mdp-workflow-backend](https://gitee.com/qingqinkj/mdp-workflow-backend) 后端

- 智能表单子系统
  1. [mdp-form-ui-web](https://gitee.com/qingqinkj/mdp-form-ui-web) 前端 
  2. [mdp-form-backend](https://gitee.com/qingqinkj/mdp-form-backend) 后端

- 内容管理子系统
  1. [mdp-arc-ui-web](https://gitee.com/qingqinkj/mdp-arc-ui-web) 前端
  2. [mdp-arc-backend](https://gitee.com/qingqinkj/mdp-arc-backend) 后端

- 短信子系统
  1. [mdp-sms-ui-web](https://gitee.com/qingqinkj/mdp-sms-ui-web) 前端
  2. [mdp-sms-backend](https://gitee.com/qingqinkj/mdp-sms-backend) 后端

- 项目管理子系统
  1. [xm-crowd-ui-web](https://gitee.com/qingqinkj/xm-crowd-ui-web) 前端众包 
  2.  [xm-ui-web](https://gitee.com/qingqinkj/xm-ui-web) 前端管理端
  3.  [xm-backend](https://gitee.com/qingqinkj/xm-backend) 后端

- 财务管理子系统
  1. [ac-core-ui-web](https://gitee.com/qingqinkj/ac-core-ui-web) 前端管理端
  2. [ac-backend](https://gitee.com/qingqinkj/ac-backend) 后端

## 📝 常见问题、操作说明

- [前端基础组件](https://gitee.com/qingqinkj/mdp-core/blob/master/docs/COMPONENTS.md)
- [前端扩展组件](https://gitee.com/qingqinkj/mdp-core/blob/master/docs/COMPONENTS.md)
- [前端公共api](https://gitee.com/qingqinkj/mdp-core/blob/master/docs/COMPONENTS.md)
- [后端公共api](https://gitee.com/qingqinkj/mdp-core/blob/master/docs/COMPONENTS.md)
- [文档主页](https://maimengcloud.com/#/openCommunity/mdp)
- [FQA](https://maimengcloud.com/#/openCommunity/fqa) 

## 💯 实践案例

1. [系统管理](https://maimengcloud.com/sys/m1/)
2. [协同办公](https://maimengcloud.com/oa/m1/)
3. [唛盟众包-网页](https://maimengcloud.com) 
3. [项目管理-网页](https://maimengcloud.com/xm/m1/) 
4. 项目管理-小程序   
   <img src="https://maimengcloud.com/img/77639c6907935d3b699f.png" alt="drawing" width="200"/>
5. [流程管理](https://maimengcloud.com/workflow/m1/)

## 🔨贡献指南

### 贡献须知

mdp 作为开源项目，离不开社区的支持，欢迎任何人修改和提出建议。贡献无论大小，你的贡献会帮助背后成千上万的使用者以及开发者，你做出的贡献也会永远的保留在项目的贡献者名单中，这也是开源项目的意义所在！

为了保证项目代码的质量与规范，以及帮助你更快的了解项目的结构，请在贡献之前阅读：

- [mdp 贡献说明](https://gitee.com/qingqinkj/mdp-core/blob/master/docs/CONTRIBUTE.md)

### 贡献步骤

1. Fork 本仓库。

2. Fork 后会在你的帐号下多了一个和本仓库一模一样的仓库，把你帐号的仓库 clone 到本地。

   注意替换掉链接中的`分支名`和`用户名`。

   如果是贡献代码，分支名填 `dev`；如果是贡献文档，分支名填 `docs`

   ```bash
   git clone -b 分支名 https://gitee.com/用户名/mdp-lcode-ui-web.git
   ```

3. 修改代码/文档，修改后提交上来。

   ```bash
   # 把修改的文件添加到暂存区
   git add .
   # 提交到本地仓库，说明你具体做了什么修改
   git commit -m '填写你做了什么修改'
   # 推送到远程仓库，分支名替换成 dev 或者 docs
   git push origin 分支名
   ```

4. 登录你的仓库，然后会看到一条 PR 请求，点击请求合并，等待管理员把你的代码合并进来。

### 项目分支说明

| 分支     | 说明                                            |
|--------|-----------------------------------------------|
| master | 主分支，受保护分支，此分支不接受 PR。在 dev 分支后经过测试没问题后会合并到此分支。 |
| dev    | 开发分支，接受 PR，PR 请提交到 dev 分支。                    |

> 目前用到的主要是 dev 和 docs 分支，接受 PR 修改，其他的分支为归档分支，贡献者可以不用管。

## 🐞 交流讨论 、反馈 BUG、提出建议等

1. 快扫描下方左侧微信二维码和我们一起交流讨论吧！（备注 唛盟-mdp 进群） 
<img src="https://maimengcloud.com/img/5ff0a747a4a1f14cf6a5.png" alt="drawing" width="200"/>

2. 唛盟微信公众号查看一些基础教程  
<img src="https://maimengcloud.com/img/f3f91bac3a3735264a66.png" alt="drawing" width="200"/>

3. 反馈 BUG、提出建议，欢迎新建：[issues](https://gitee.com/qingqinkj/mdp-lcode-ui-web/issues)，开发人员会不定时查看回复。
4. 参与贡献，请查看[贡献指南](#🔨贡献指南)。

## 💲 打赏
 **感谢所有赞赏以及参与贡献的小伙伴，你们的支持是我们不断更新前进的动力！微信扫一扫，赏杯咖啡呗！**    
 <img src="https://maimengcloud.com/img/97094cc1553fe0b0046c.jpg" alt="drawing" width="300"/>



## 🔔 精品项目推荐

| 项目名称              | 项目地址                                                                                    | 项目介绍                                          |
|----------------------|---------------------------------------------------------------------------------------------|--------------------------------------------------|
| SpringBoot_v2        | [https://gitee.com/bdj/SpringBoot_v2](https://gitee.com/bdj/SpringBoot_v2) -               | 基于springboot的一款纯净脚手架                     |